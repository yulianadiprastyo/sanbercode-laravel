<?php
    require('animal.php');
    require('ape.php');
    require('frog.php');

    $sheep = new Animal("shaun");

    echo "SOAL 1 <br>";
    echo $sheep->name . "<br>"; // "shaun"
    echo $sheep->legs . "<br>"; // 2
    echo $sheep->cold_blooded . "<br>";// false
    echo "<br>";

    echo "SOAL 2 <br>";
    // index.php
    $sungokong = new Ape("kera sakti");
    echo $sungokong->name . "<br>";
    echo $sungokong->legs . "<br>";
    $sungokong->yell(); // "Auooo"
    echo "<br>";

    $kodok = new Frog("buduk");
    echo $kodok->name . "<br>";
    echo $kodok->legs . "<br>";
    $kodok->jump() ; // "hop hop"

    // NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
?>